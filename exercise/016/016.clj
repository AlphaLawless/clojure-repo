; (defn distance
  ; [value]
  ; (format "%d minutos"
  ; (* value 2)))

(def km (read-string (readline)))
(def minutos (/ (* 60 km) 30))

(printf "%d minutos\n" minutos)
