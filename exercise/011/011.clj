(defn sphere
  [radio]
  (def pi 3.14159)
  (def R3 (Math/pow radio 3))
  (str "VOLUME = "
          (format "%.3f"
          (double
          (/
          (* 4 R3 pi)
          3)))))
